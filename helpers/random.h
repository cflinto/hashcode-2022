#ifndef RANDOM_H
#define RANDOM_H

#include <type_traits>
#include <random>
#include <algorithm>

#include "misc.h"

// ***********************
// * RANDOM HELPER CLASS *
// ***********************
//
//   Offer a simple initialisation and interfaces to usual random
// functionality.
//
//  Currently it only initializes and offers easy generator access,
// to use in stdlib's methods.

class Random
{
    // The generator we're using. Allow to easily swap it to test out.
    using generator_t = std::mt19937_64;
public:
    // Initialize the generator
    static void Init() {
        std::random_device rd;
        _generator = generator_t(rd());
    }

    // Get the generator for external use (mostly in other helpers)
    static generator_t& generator() { return _generator; }

    template<typename T>
    static auto range(T min, T max)
    -> typename std::enable_if<std::is_integral<T>::value, T>::type
    {
        std::uniform_int_distribution<T> d(min, max);
        return d(generator());
    }

    template<typename T>
    static auto range(T min, T max)
    -> typename std::enable_if<std::is_floating_point<T>::value, T>::type
    {
        std::uniform_real_distribution<T> d(min, max);
        return d(generator());
    }

    template<typename T>
    static auto distinct_integers(size_t n, T min, T max)
    -> typename std::enable_if<std::is_integral<T>::value, std::vector<T>>::type
    {
        std::vector<T> pick;

        size_t range = max-min+1;

        pick.resize(range);

        n = std::min(n, range);

        std::iota(std::begin(pick), std::end(pick), min);
        std::shuffle(std::begin(pick), std::end(pick), _generator);

        pick.erase(pick.begin()+n, pick.end());
        return pick;
    }

    template<typename T>
    static auto distinct_pairs(size_t n, T max)
    -> typename std::enable_if<std::is_integral<T>::value, std::vector<std::pair<T,T>>>::type
    {
        size_t sub_max = max*(max+1)/2 - 1;
        std::vector<T> indices = Random::distinct_integers<T>(n, 0, sub_max);
        std::vector<std::pair<T,T>> pairs;

        pairs.reserve(indices.size());

        std::transform(std::begin(indices), std::end(indices),
                       std::back_inserter(pairs), indexed_pair<T>);

        return pairs;
    }

    static bool boolean(double p = 0.5)
    {
        std::bernoulli_distribution d(p);
        return d(generator());
    }

private:
    // The actual stored generator.
    static generator_t _generator;
};

#endif // RANDOM_H
