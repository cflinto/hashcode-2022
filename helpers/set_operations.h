#ifndef SET_OPERATIONS_H
#define SET_OPERATIONS_H

#include <algorithm>

#include <boost/iterator/function_output_iterator.hpp>

#include "random.h"

// **************************
// * SET OPERATIONS HELPERS *
// **************************
//
// Set operations helpers functions acting as proxies for the stdlib's ones.
//
// Currently :
//   - inclusion test.
//   - union, intersection, and differences operations.
//   - union, intersection, and differences cardinality operations.
//   - (reverse) sort and random shuffle.


// Inclusion test for generic containers
template<class C1, class C2>
auto includes(const C1& c1, const C2& c2) {
    return std::includes(std::begin(c1), std::end(c1),
                         std::begin(c2), std::end(c2));
}

template<class C, typename T>
auto find(const C& c, const T& element) {
    return std::find(std::begin(c), std::end(c), element);
}

// Inclusion test of an element in a generic containers
template<class C, typename T>
bool contains(const C& set, const T& element) {
    return find(set, element) != std::end(set);
}

template<class C, typename T>
auto remove(C& c, const T& val) {
    return std::remove(std::begin(c), std::end(c), val);
}

template<class C, typename T>
auto iota(C& c, const T& init) {
    return std::iota(std::begin(c), std::end(c), init);
}

// Difference operation for generic containers
// It adds the difference of c1 and c2 at the back of c3.
template<class C1, class C2, class C3>
auto set_difference(const C1& c1, const C2& c2, C3& c3) {
    return std::set_difference(std::begin(c1), std::end(c1),
                               std::begin(c2), std::end(c2),
                               std::back_inserter(c3)
                               );
}

// Difference cardinality operation for generic containers
// It adds the cardinality of the difference of c1 and c2 to counter.
template<class C1, class C2, class COUNT>
auto set_difference_count(const C1& c1, const C2& c2, COUNT& count) {
    auto unary = [&count](const typename C1::value_type& e){count++;};
    auto counter = boost::make_function_output_iterator(unary);
    std::set_difference(std::begin(c1), std::end(c1),
                        std::begin(c2), std::end(c2),
                        counter
                       );
}

// Intersection operation for generic containers
// It adds the intersection of c1 and c2 at the back of c3.
template<class C1, class C2, class C3>
auto set_intersection(const C1& c1, const C2& c2, C3& c3) {
    return std::set_intersection(std::begin(c1), std::end(c1),
                                 std::begin(c2), std::end(c2),
                                 std::back_inserter(c3));
}

// Intersection cardinality operation for generic containers
// It adds the cardinality of the intersection of c1 and c2 to counter.
template<class C1, class C2, class COUNT>
auto set_intersection_count(const C1& c1, const C2& c2, COUNT& count) {
    auto unary = [&count](const typename C1::value_type& e){count++;};
    auto counter = boost::make_function_output_iterator(unary);
    return std::set_intersection(std::begin(c1), std::end(c1),
                                 std::begin(c2), std::end(c2),
                                 counter);
}

// Symmetric difference operation for generic containers
// It adds the symmetric difference of c1 and c2 at the back of c3.
template<class C1, class C2, class C3>
auto set_symmetric_difference(const C1& c1, const C2& c2, C3& c3) {
    return std::set_symmetric_difference(std::begin(c1), std::end(c1),
                                        std::begin(c2), std::end(c2),
                                        std::back_inserter(c3));
}

// Symmetric difference cardinality operation for generic containers
// It adds the cardinality of the symmetric difference of c1 and c2 to counter.
template<class C1, class C2, class COUNT>
auto set_symmetric_difference_count(const C1& c1, const C2& c2, COUNT& count) {
    auto unary = [&count](const typename C1::value_type& e){count++;};
    auto counter = boost::make_function_output_iterator(unary);
    return std::set_symmetric_difference(std::begin(c1), std::end(c1),
                                        std::begin(c2), std::end(c2),
                                        counter);
}

// Union operation for generic containers
// It adds the union of c1 and c2 at the back of c3.
template<class C1, class C2, class C3>
auto set_union(const C1& c1, const C2& c2, C3& c3) {
    return std::set_union(std::begin(c1), std::end(c1),
                          std::begin(c2), std::end(c2),
                          std::back_inserter(c3));
}

// Union cardinality operation for generic containers
// It adds the cardinality of the union of c1 and c2 to counter.
template<class C1, class C2, class COUNT>
auto set_union_count(const C1& c1, const C2& c2, COUNT& count) {
    auto unary = [&count](const typename C1::value_type& e){count++;};
    auto counter = boost::make_function_output_iterator(unary);
    return std::set_union(std::begin(c1), std::end(c1),
                          std::begin(c2), std::end(c2),
                          counter);
}

// Sort operation : simply sort a generic container
template<class C>
auto sort(C& c) {
    return std::sort(std::begin(c), std::end(c));
}

// Reverse sort operation : simply sort a generic container in descending order
template<class C>
auto rsort(C& c) {
    return std::sort(std::rbegin(c), std::rend(c));
}

// Shuffle operation : shuffles a generic container
template<class C>
auto shuffle(C& c) {
    return std::shuffle(std::begin(c), std::end(c), Random::generator());
}

template<class C>
auto unique(C& c) {
    return std::unique(std::begin(c), std::end(c));
}


#endif // SET_OPERATIONS_H
