#ifndef MISC_H
#define MISC_H

#include <algorithm>
#include <cmath>

// *****************
// * MISC. HELPERS *
// *****************
//
//   Various helpers functions, acting as proxies to stdlib's ones.
//   So we write simpler (quicker & less error-prone) code, still levaraging
// the stdlib's good implementations.
//
//   Currently :
//     - variadic min and max
//

template<typename T>
auto indexed_pair(T index)
-> typename std::enable_if<std::is_integral<T>::value, std::pair<T,T>>::type
{

    T q = std::floor(std::sqrt(8*index+1)/2.0 + 0.5);
    T p = (index) - q * ((q-1)/2.0);

    return {p,q};
}


// Variadic min base-case
template<typename T>
T min(const T& t)
{
    return t;
}

// Variadic min function
template<typename T0, typename... T>
T0 min(const T0& first, const T&... args)
{
    return std::min(first, min<T...>(args...));
}

// Variadic max base-case
template<typename T>
T max(const T& t)
{
    return t;
}

// Variadic max function
template<typename T0, typename... T>
T0 max(const T0& first, const T&... args)
{
    return std::max(first, max<T...>(args...));
}

// gives - at compile-time - log2(x) (kinda)
template<typename I>
constexpr auto log2(const I& x)
-> typename std::enable_if<std::is_unsigned<I>::value,size_t>::type
{
    return x < 2 ? 1 : 1 + log2(x >> 1);
}

// Gives the correct minimum-sized unsigned type for indexing 'N' values
// usage : index_for<N>::type
template<size_t N>
struct index_for
{
private:
    using type64 = typename std::enable_if  <log2(N) <= 64, u_int64_t>::type;
    using type32 = typename std::conditional<log2(N) <= 32, u_int32_t, type64>::type;
    using type16 = typename std::conditional<log2(N) <= 16, u_int16_t, type32>::type;
    using type8  = typename std::conditional<log2(N) <= 8,  u_int8_t, type16>::type;

public:
    using type = type8;
};

template<typename ... Types>
using pack_first = std::tuple_element<0, std::tuple<Types...>>;

template<typename First, typename ... Types>
struct is_same;

template<typename First, typename Second, typename ... Types>
struct is_same<First, Second, Types...>
{
private:
    // The recursion
    constexpr static bool recursion = is_same<Second, Types...>::value;
    // The case of more types : we have to recurse
public:
    //value now is true iff all passed types are the same
    constexpr static bool value = std::is_same<First, Second>::value ?
        recursion :
        false;
};

template<typename First>
struct is_same<First>
{
    constexpr static bool value = true;
};


#endif // MISC_H
