#ifndef STARPU_HELPERS_H
#define STARPU_HELPERS_H

#include <array>

#include "starpu.h"

#include "misc.h"

template <typename ... Args, typename = typename std::enable_if<is_same<starpu_data_access_mode, Args...>::value, void>::type>
starpu_codelet make_CPU_codelet(const char* const name, void (*f)(void**,void*), Args...access_modes)
{
    starpu_codelet ret;
    starpu_codelet_init(&ret);

    ret.name = name;
    ret.nbuffers = sizeof...(Args);
    std::array<starpu_data_access_mode, sizeof...(Args)> modes_temp {access_modes...};
    std::copy(std::begin(modes_temp), std::end(modes_temp), std::begin(ret.modes));

    ret.cpu_funcs[0] = f;

    return ret;
}

template <typename ... Args, typename = typename std::enable_if<is_same<starpu_data_access_mode, Args...>::value, void>::type>
starpu_codelet make_CPU_codelet(void (*f)(void**,void*), Args...access_modes)
{
    return make_CPU_codelet(nullptr, f, access_modes...);
}

#endif // STARPU_HELPERS_H
