#ifndef POPULATION_HPP
#define POPULATION_HPP

#include <vector>

#include "set_operations.h"

template<class S>
class Population
{
private:
    std::vector<S> data;
    using data_t = decltype(data);

public:
    using iterator = typename data_t::iterator;
    using const_iterator = typename data_t::const_iterator;
    using reverse_iterator = typename data_t::reverse_iterator;
    using const_reverse_iterator = typename data_t::const_reverse_iterator;
    using reference = typename data_t::reference;
    using const_reference = typename data_t::const_reference;
    using size_type = typename data_t::size_type;
    using value_type = typename data_t::value_type;

private:
    size_type _alive = 0;

public:
    iterator begin()  { return data.begin(); }
    iterator end()    { return data.begin() + alive(); }
    reverse_iterator rbegin() { return std::make_reverse_iterator(data.begin() + alive()); }
    reverse_iterator rend()   { return std::make_reverse_iterator(data.begin()); }
    const_iterator cbegin() { return data.cbegin(); }
    const_iterator cend()   { return data.cbegin() + alive(); }
    const_reverse_iterator crbegin() { return std::make_reverse_iterator(data.cbegin() + alive()); }
    const_reverse_iterator crend()   { return std::make_reverse_iterator(data.cbegin()); }
    reference front()  { return data.front(); }

    // TODO: think about a good name...
    reference to_write() { return data[_alive++]; }

    size_type alive() { return _alive; }

    auto resize(size_type size) { return data.resize(size); }

    // Offer the read-only signature only, to avoid headaches
    const_reference operator [](size_type pos) const { return data[pos]; };

    void kill(size_type n) { _alive -= n; }
    void kill(iterator from) { _alive -= std::distance(from, end()); }

    void sort() { rsort(*this); }
};

template <class S>
using population_iterator = typename Population<S>::iterator;

#endif //POPULATION_HPP
