#ifndef SCORER_H_INCLUDED
#define SCORER_H_INCLUDED

#include <cassert>

template<class P, class S>
class Scorer
{
	using Score = size_t; // the scorer sets the nature of Score

	static const Score __ScoreType;

	public:

    static Score compute(const P& problem, const S& solution)
    {
    	assert(0); // children need to implement
    	Score s=0;
    	return s;
    };
};

#endif // SCORER_H_INCLUDED

