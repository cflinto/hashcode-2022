// A solver : 0-N solutions => 1-N (better) solutions

#ifndef SOLVER_H_INCLUDED
#define SOLVER_H_INCLUDED

#include <iostream>
#include <fstream>
#include <chrono>

#include "population.hpp"

// Temporarily put those here
enum GeneticSelection : unsigned short
{
    None,
    RouletteWheel,
    GaripWheel
};

struct GeneticParameters
{
    // Behavioral
    GeneticSelection selection = GaripWheel; // The probabilistic selection to use

    // Quantification
    size_t target_size = 100;   // The target population size
    float elitism      = 0.25f; // The proportion of population to select by elitism
    float probabilism  = 0.25f; // The proportion of population to select by the probabilistic selection
    float crossoverism = 0.4f;  // The proportion of new population to fill by crossovers
    float mutationism  = 0.4f;  // The proportion of new population to fill by mutations
    float heuristism   = 0.5f;  // When using random, use this proportion of "heuristic random"
    // The rest will be filled by random !

    float probabilist_param = 0.0f; // A parameter used by some probabilistic selections.
};

//template<class Solution, class Scorer>

template<class P, class S>
class Solver
{
public:
    using clock = std::chrono::steady_clock;
    using time_point = std::chrono::time_point<clock>;

    Solver(const std::string problem_path, const std::string& checkpoint_id, const GeneticParameters& params) :
        problem_path(problem_path),
        problem_id(problem_path),
        parameters(params)
    {
        size_t i = problem_id.find_last_of('/');

        if(i != std::string::npos)
            problem_id = problem_id.substr(i+1);
        problem_id = problem_id.substr(0,1);

        checkpoint_path = problem_id + '.' + checkpoint_id;
    }

    // Any initialization step (for instance, initial population)
    virtual void initialize() = 0;

    // One step in the iterative optimization
    virtual void step() = 0;

    // Communication : send some good ones to other solvers, send best one to the driver, etc...
    void process()
    {
        time_point now = clock::now();
        if((now - last_checkpoint) > std::chrono::seconds(60))
        {
            last_checkpoint = now;
            checkpoint(checkpoint_path);
        }
    }

    const S& get_best() const { return population[0]; }

    virtual void parse_problem(std::istream& in) = 0;
    void parse_problem(const std::string& filename){
        std::ifstream input_file;

        input_file.open(filename);

        if(!input_file.is_open())
        {
            std::cerr << "Error opening " << filename << " !" << std::endl;
            exit(EXIT_FAILURE);
        }

        std::cout << "Parsing problem ... ";

        this->parse_problem(input_file);

        std::cout << "Done !" << std::endl;
    }
    void parse_problem() { return parse_problem(problem_path); }

    virtual void dump_solution(std::ostream& out, const S& solution) const = 0;
    void dump_solution(const std::string& filename, const S& solution) const {

        std::ofstream output_file;
        output_file.open(filename);

        if(!output_file.is_open())
        {
            std::cerr << "Error opening " << filename << " !" << std::endl;
            exit(EXIT_FAILURE);
        }

        dump_solution(output_file, solution);
    }

    void dump_best(std::ostream& out)     { return dump_solution(out, get_best()); }
    void dump_best(const std::string& filename) { return dump_solution(filename, get_best()); }
    void dump_best() { return dump_best("Champion_" + problem_id); }

    virtual bool has_parse_solution() { return false; }
    virtual void parse_solution(std::istream& in, S& solution) const { return; }

    void checkpoint(std::ostream& out)
    {
        out << population.alive() << std::endl;

        for(S& solution : population)
        {
            dump_solution(out, solution);
            out << solution.score << std::endl;
        }

    }

    void checkpoint(const std::string& checkpoint_path)
    {
        std::ofstream checkpoint_file;
        checkpoint_file.open(checkpoint_path);

        if(!checkpoint_file.is_open())
        {
            std::cerr << "Error opening " << checkpoint_path << " !" << std::endl;
            exit(EXIT_FAILURE);
        }

        std::cout << "Checkpointing population ... ";

        checkpoint(checkpoint_file);

        std::cout << "Done !" << std::endl;
    }

    bool recover(std::istream& in)
    {
        if(!has_parse_solution())
        {
            std::cerr << "WARNING: parse_solution not implemented: skipping recovery." << std::endl;
            return false;
        }

        std::cout << "Recovering population from checkpoint ... ";

        size_t n;
        in >> n;
        population.resize(n);

        for(size_t i = 0; i < n; i++)
        {
            parse_solution(in, population.to_write());
        }

        std::cout << "Done !" << std::endl;

        return true;
    }

    bool recover(const std::string& checkpoint_path)
    {
        std::ifstream checkpoint_file;
        checkpoint_file.open(checkpoint_path);

        if(!checkpoint_file.is_open())
        {
            std::cerr << "WARNING: Error opening file at " << checkpoint_path << ": skipping recovery." << std::endl;
            return false;
        }

        if(!has_parse_solution())
        {
            std::cerr << "WARNING: parse_solution not implemented: skipping recovery." << std::endl;
            return false;
        }

        bool r = this->recover(checkpoint_file);
        if(r)
        {
            population.sort();
        }

        return r;
    }
    bool recover() { return recover(checkpoint_path); }

    const GeneticParameters& get_parameters() { return parameters; }
    void set_parameters(const GeneticParameters& p) { parameters = p; }

    Population<S> population;

protected:

    std::string problem_id;
    std::string checkpoint_path;
    std::string problem_path;

    GeneticParameters parameters;
    time_point last_checkpoint = clock::now();
    P problem;
	// get 
};

#endif // SOLVER_H_INCLUDED

