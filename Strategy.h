// Handles the execution of a strategy
// always specific to the problem

#ifndef STRATEGY_H_INCLUDED
#define STRATEGY_H_INCLUDED

#include <iostream>
#include <streambuf>
#include <starpu.h>
#include <starpu_mpi.h>

#include "starpu_helpers.h"
#include "genetic/genetic_algorithm.hpp"

struct streambuffer: public std::streambuf
{
   streambuffer(char* p, size_t size)
   {
       setp(p, p + size + 1);
       setg(p, p, p + size + 1);
   }

   size_t written() { return pptr()-pbase(); }
};

struct Champion_ID {
    size_t score;
    int world_rank;
    int worker_id;
};

// We initiate the reduction with a "null" ID
void champion_id_neutral(void** buffers, void* arg)
{
    Champion_ID* id = (Champion_ID*)STARPU_VARIABLE_GET_PTR(buffers[0]);

    id->score = std::numeric_limits<size_t>::min();
    id->world_rank = -1;
    id->worker_id  = -1;
}

// We reduce to the best ID
void champion_id_redux(void** buffers, void* arg)
{
    Champion_ID* id_dst = (Champion_ID*)STARPU_VARIABLE_GET_PTR(buffers[0]);
    Champion_ID* id_src = (Champion_ID*)STARPU_VARIABLE_GET_PTR(buffers[1]);

    if(id_dst->score < id_src->score)
    {
        *id_dst = *id_src;
    }
}

template<class Solution, class Problem, class Solver>
// TODO : template Solution is mandatory ?
class Strategy
{
    public:

    int world_rank;
    int worker_id;

    starpu_codelet codeletStep;
    starpu_codelet codeletProcess;
    starpu_codelet codeletInitialize;
    starpu_codelet codeletIntake;
    starpu_codelet codeletChampionID;
    starpu_codelet codeletDumpChampion;

    starpu_data_handle_t handleSolver;
    starpu_data_handle_t handleBuffer;
    starpu_data_handle_t handleParams;

    static constexpr size_t buffer_length = 1ul << 24ul;

    Strategy(int world_rank, int worker_id, int total_workers) :
        world_rank(world_rank), worker_id(worker_id)
    {
        codeletStep = make_CPU_codelet("step",
                                       stepFunction,
                                       STARPU_RW);

        codeletProcess = make_CPU_codelet("process",
                                          processFunction,
                                          STARPU_RW, STARPU_W);

        codeletInitialize = make_CPU_codelet("initialize",
                                             initializeFunction,
                                             STARPU_W,
                                             STARPU_R);

        codeletIntake = make_CPU_codelet("intake",
                                         intakeFunction,
                                         STARPU_RW, STARPU_R);

        codeletChampionID = make_CPU_codelet("championID",
                                             championIDFunction,
                                             STARPU_REDUX,
                                             STARPU_RW);

        codeletDumpChampion = make_CPU_codelet("dumpChampion",
                                               dumpChampionFunction,
                                               STARPU_RW);

        starpu_variable_data_register(&handleSolver, -1, (uintptr_t)0, sizeof(Solver));
        starpu_vector_data_register(&handleBuffer, -1, (uintptr_t)0, buffer_length, sizeof(char));
        starpu_variable_data_register(&handleParams, -1, (uintptr_t)0, sizeof(GeneticParameters));

        int world_size = starpu_mpi_world_size();

        int tag_solver = world_rank * total_workers + worker_id;
        int tag_buffer = world_size * total_workers + world_rank * total_workers + worker_id;
        int tag_params = 2 * world_size * total_workers + world_rank * total_workers + worker_id;

        starpu_mpi_data_register(handleSolver, tag_solver, world_rank);
        starpu_mpi_data_register(handleBuffer, tag_buffer, world_rank);
        starpu_mpi_data_register(handleParams, tag_params, world_rank);
    }

    static size_t buffer_solution(const Solver& solver, char buffer[], const Solution& solution)
    {
        streambuffer sbuf(buffer, buffer_length);
        std::ostream stream(&sbuf);

        solver.dump_solution(stream, solution);
        stream << solution.score << std::endl;
        stream.flush();

        return sbuf.written();
    }

    static void unbuffer_solution(const Solver& solver, char buffer[], Solution& solution)
    {
        streambuffer sbuf(buffer, buffer_length);
        std::istream stream(&sbuf);

        solver.parse_solution(stream, solution);
    }

    void get_solution(const Solver& solver, Solution& solution)
    {
        starpu_data_acquire(handleBuffer, STARPU_R);
        char* buffer = reinterpret_cast<char*>(starpu_vector_get_local_ptr(handleBuffer));


        unbuffer_solution(solver, buffer, solution);

        starpu_data_release(handleBuffer);
    }

    void initialize(const std::string& problem_path)
    {
        starpu_mpi_task_insert(MPI_COMM_WORLD,
                               &codeletInitialize,
                               STARPU_W, handleSolver,
                               STARPU_R, handleParams,
                               STARPU_VALUE, problem_path.data(), problem_path.size() + 1,
                               STARPU_EXECUTE_ON_NODE, world_rank,
                               STARPU_EXECUTE_ON_WORKER, worker_id,
                               0);
    }

    void step()
    {
        starpu_mpi_task_insert(MPI_COMM_WORLD,
                               &codeletStep,
                               STARPU_RW, handleSolver,
                               STARPU_EXECUTE_ON_NODE, world_rank,
                               STARPU_EXECUTE_ON_WORKER, worker_id,
                               0);
    }

    void process()
    {
        starpu_mpi_task_insert(MPI_COMM_WORLD,
                               &codeletProcess,
                               STARPU_RW, handleSolver,
                               STARPU_W,  handleBuffer,
                               STARPU_EXECUTE_ON_NODE, world_rank,
                               STARPU_EXECUTE_ON_WORKER, worker_id,
                               0);
    }

    void intake(starpu_data_handle_t intake_buffer)
    {
        starpu_mpi_task_insert(MPI_COMM_WORLD,
                               &codeletIntake,
                               STARPU_RW, handleSolver,
                               STARPU_R,  intake_buffer,
                               STARPU_EXECUTE_ON_NODE, world_rank,
                               STARPU_EXECUTE_ON_WORKER, worker_id,
                               0);
    }

    void prepareChampionID(starpu_data_handle_t& handleChampion)
    {
        starpu_mpi_task_insert(MPI_COMM_WORLD,
                               &codeletChampionID,
                               STARPU_REDUX, handleChampion,
                               STARPU_RW, handleSolver,
                               STARPU_VALUE, &world_rank, sizeof(world_rank),
                               STARPU_VALUE, &worker_id, sizeof(worker_id),
                               STARPU_EXECUTE_ON_NODE, world_rank,
                               STARPU_EXECUTE_ON_WORKER, worker_id,
                               0);
    }

    void dumpChampion()
    {
        starpu_mpi_task_insert(MPI_COMM_WORLD,
                               &codeletDumpChampion,
                               STARPU_RW, handleSolver,
                               STARPU_EXECUTE_ON_NODE, world_rank,
                               STARPU_EXECUTE_ON_WORKER, worker_id,
                               0);
    }
private:
    static void stepFunction(void** buffers, void* arg)
    {
        Solver *ms = (Solver*)STARPU_VARIABLE_GET_PTR(buffers[0]);

        ms->step();
    }

    static void processFunction(void *buffers[], void *arg)
    {
        Solver *ms = (Solver*)STARPU_VARIABLE_GET_PTR(buffers[0]);
        char* buffer = (char*)STARPU_VECTOR_GET_PTR(buffers[1]);

        ms->process();

        size_t written = buffer_solution(*ms, buffer, ms->get_best());

        // Tell StarPU where the actual buffer is and what its practical size is
        ((struct starpu_vector_interface *)buffers[1])->nx  = written;
    }

    static void initializeFunction(void *buffers[], void *arg)
    {
        Random::Init();
        GeneticParameters* params = (GeneticParameters*)STARPU_VARIABLE_GET_PTR(buffers[1]);
        // TODO : maybe a handle would be better, dunno
        char c_str[20000];
        starpu_codelet_unpack_args(arg, c_str);
        std::string problem_path(c_str);

        std::string checkpoint_id = std::to_string(starpu_mpi_world_rank()) + "."
                                  + std::to_string(starpu_worker_get_id());

        Solver* solver = new Solver(problem_path, checkpoint_id, *params);
        solver->parse_problem();
        solver->recover();
        solver->initialize();

        ((starpu_variable_interface*)buffers[0])->ptr = (uintptr_t)solver;
    }

    static void intakeFunction(void *buffers[], void* arg)
    {
        Solver *ms = (Solver*)STARPU_VARIABLE_GET_PTR(buffers[0]);
        char* buffer = (char*)STARPU_VECTOR_GET_PTR(buffers[1]);

        // Number to intake
        size_t n = 1;
        size_t room = ms->get_parameters().target_size - ms->population.alive();
        size_t to_kill = n > room ? n-room : 0;
        ms->population.kill(to_kill);

        unbuffer_solution(*ms, buffer,
                          ms->population.to_write());
        ms->population.sort();
    }

    // We set the local champion's ID
    static void championIDFunction(void** buffers, void* arg)
    {
        Champion_ID* local_id = (Champion_ID*)STARPU_VARIABLE_GET_PTR(buffers[0]);
        Solver* solver = (Solver*)STARPU_VARIABLE_GET_PTR(buffers[1]);

        local_id->score = solver->get_best().score;

        starpu_codelet_unpack_args(arg,
                                   &(local_id->world_rank),
                                   &(local_id->worker_id));
    }

    static void dumpChampionFunction(void** buffers, void* arg)
    {
        Solver* solver = (Solver*)STARPU_VECTOR_GET_PTR(buffers[0]);
        solver->dump_best();
    }
};


#endif // STRATEGY_H_INCLUDED

