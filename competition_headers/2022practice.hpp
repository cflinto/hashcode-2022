#include "genetic/genetic_algorithm.hpp"

#include <set>
#include <map>
#include <fstream>
#include <random>
#include <algorithm>

#include "Solution.h"
#include "helpers/set_operations.h"
#include "helpers/random.h"
#include "helpers/misc.h"
#include "Problem.h"
#include "Scorer.h"


struct Client
{
    std::vector<uint16_t> like;
    std::vector<uint16_t> hate;
};
struct Problem2022practice : public ProblemBase
{
    std::vector<uint16_t> ingredients_id;
    std::vector<std::string> ingredients_str;
    std::vector<Client> all_clients;
};


struct Solution2022practice : SolutionBase
{
    std::vector<uint16_t> pizza;
};

// bool operator<(const Solution2022practice& a, const Solution2022practice& rhs)
// {
//     return a.score < rhs.score || (a.score == rhs.score &&  a.pizza.size() < rhs.pizza.size());
// }
bool operator==(const Solution2022practice& a, const Solution2022practice& b)
{
    return a.score == b.score && a.pizza == b.pizza;
}

class Scorer2022practice : public Scorer<Problem2022practice, Solution2022practice>
{
	using Score = short; // the scorer sets the nature of Score

	static const Score __ScoreType;

	public:

    static Score compute(const Problem2022practice& problem, const Solution2022practice& solution)
	{
		Score r = 0;
		
        int counter_hate;
        for(const Client& c : problem.all_clients)
        {
            counter_hate = 0;
            set_intersection_count(solution.pizza, c.hate, counter_hate);
            if(includes(solution.pizza, c.like) && !counter_hate)
            {
                r++;
            }
        }
        
		return r;
	}
};

class GeneticAlgorithm : public GeneticAlgorithmBase<Problem2022practice, Solution2022practice>
{
    using GeneticAlgorithmBase<Problem2022practice,Solution2022practice>::GeneticAlgorithmBase;

    bool has_heuristic() final { return false; }

    void heuristic(Solution2022practice& to_randomize) final{

        int is_inside;
        to_randomize.pizza.clear();
        for(const Client& c : problem.all_clients)
        {
            // if(Random::boolean()){ continue; }
            is_inside = 0;
            set_intersection_count(to_randomize.pizza, c.hate, is_inside);
            if(!is_inside)
            {
                set_difference(c.like, to_randomize.pizza, to_randomize.pizza);
            }
            sort(to_randomize.pizza);
        }

        to_randomize.score = Scorer2022practice::compute(problem, to_randomize);
    }

    bool has_random() final { return true; }

    void random(Solution2022practice& to_randomize) final{


        to_randomize.pizza.clear();
        for(uint16_t i = 0; i < problem.ingredients_id.size(); i++)
        {
            if(Random::boolean())
            {
                to_randomize.pizza.emplace_back(i);
            }
        }

        to_randomize.score = Scorer2022practice::compute(problem, to_randomize);
    }

    bool has_crossover() final { return true; }

    void crossover(const Solution2022practice &parent1, const Solution2022practice &parent2, Solution2022practice& child1, Solution2022practice& child2) final
    {
        std::vector<uint16_t> sym_diff_pizza;

        child1.pizza.clear();
        child2.pizza.clear();

        set_intersection(parent1.pizza, parent2.pizza, child1.pizza);
        set_intersection(parent1.pizza, parent2.pizza, child2.pizza);

        set_symmetric_difference(parent1.pizza, parent2.pizza, sym_diff_pizza);
        shuffle(sym_diff_pizza);

        for(uint16_t i = 0; i < sym_diff_pizza.size()/2; ++i)
        {
            child1.pizza.emplace_back(sym_diff_pizza[i]);
        }
        sort(child1.pizza);

        for(uint16_t i = sym_diff_pizza.size()/2; i < sym_diff_pizza.size(); ++i)
        {
            child2.pizza.emplace_back(sym_diff_pizza[i]);
        }
        sort(child2.pizza);

        child1.score = Scorer2022practice::compute(problem, child1);
        child2.score = Scorer2022practice::compute(problem, child2);
    }

    bool has_mutation() final { return true; }

    void mutation(const Solution2022practice &original, Solution2022practice& mutant) final {

        mutant = original;
        if(Random::boolean() && mutant.pizza.size() < problem.ingredients_str.size())
        {
            std::vector<uint16_t> pizza_sym_diff;
            set_difference(problem.ingredients_id, mutant.pizza, pizza_sym_diff);

            uint16_t to_add = pizza_sym_diff[Random::range((uint16_t)0,uint16_t(pizza_sym_diff.size()-1))];
            mutant.pizza.emplace_back(std::move(to_add));
        }
        else
        {
            uint16_t to_remove = mutant.pizza[Random::range((uint16_t)0,uint16_t(mutant.pizza.size()-1))];
            mutant.pizza.erase(remove(mutant.pizza, std::move(to_remove)), mutant.pizza.end());
        }

        sort(mutant.pizza);
        mutant.score = Scorer2022practice::compute(problem, mutant);
    }

    void dump_solution(std::ostream& out, const Solution2022practice& solution) const final
    {
        out << solution.pizza.size();

        for(const uint16_t& idx : solution.pizza)
        {
            out << " " << problem.ingredients_str[idx];
        }

        out << std::endl;
    }

    bool has_parse_solution() final { return true; }

    void parse_solution(std::istream& in, Solution2022practice& solution) const final
    {   uint16_t size;
        in >> size;

        solution.pizza.clear();
        solution.pizza.reserve(size);
        std::string ingr;
        uint16_t ingr_id;
        for(uint16_t i = 0; i < size; ++i)
        {
            in >> ingr;
            ingr_id = find(problem.ingredients_str, ingr) - problem.ingredients_str.begin();
            
            solution.pizza.emplace_back(ingr_id);
        }
        solution.score = Scorer2022practice::compute(problem, solution);
    }

    
    void parse_problem(std::istream& in) final
    {
        uint16_t n_clients;
        in >> n_clients;
        problem.all_clients.resize(n_clients);
        uint16_t max_ingredients = 0;

        uint16_t idx;
        uint16_t j;
        std::string ingredient;
        uint16_t n_ingredients;
        for(uint16_t i = 0; i < n_clients; i++)
        {

            in >> n_ingredients;

            for(j = 0; j < n_ingredients; j++)
            {
                in >> ingredient;
                if(!contains(problem.ingredients_str,ingredient))
                {
                    problem.ingredients_str.emplace_back(ingredient);
                    max_ingredients++;
                }
                idx = find(problem.ingredients_str, ingredient) - problem.ingredients_str.begin();

                problem.all_clients[i].like.emplace_back(idx);
            }

            sort(problem.all_clients[i].like);

            in >> n_ingredients;

            for(j = 0; j < n_ingredients; j++)
            {
                in >> ingredient;
                if(!contains(problem.ingredients_str,ingredient))
                {
                    problem.ingredients_str.emplace_back(ingredient);
                    max_ingredients++;
                }
                idx = find(problem.ingredients_str, ingredient) - problem.ingredients_str.begin();

                problem.all_clients[i].hate.emplace_back(idx);
            }

            sort(problem.all_clients[i].hate);

        }
        problem.ingredients_id.reserve(max_ingredients);
        for(uint16_t idx = 0; idx < max_ingredients; ++idx)
        {
            problem.ingredients_id.emplace_back(idx);
        }
    }
};

