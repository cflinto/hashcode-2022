#include "genetic/genetic_algorithm.hpp"

#include <set>
#include <map>
#include <fstream>
#include <random>
#include <algorithm>
#include <sstream>

#include "Solution.h"
#include "set_operations.h"
#include "random.h"
#include "misc.h"
#include "Problem.h"
#include "Scorer.h"

using tag_t = size_t;
using tags_t = std::vector<tag_t>;
// The 2019 problem explicitely tells us we have N <= 10^5 pictures
using pic_ind_t = index_for<100000ul>::type;

struct Picture
{
    bool horizontal;
    tags_t tags;
};

struct Problem2019 : public ProblemBase
{
    std::map<std::string, tag_t> tags_id;
    std::vector<Picture> pictures;
    std::vector<pic_ind_t>  verticals;
    std::vector<pic_ind_t>  horizontals;
};

struct Slide
{
    bool is_horizontal;
    pic_ind_t pic1;
    pic_ind_t pic2;
};

bool operator==(const Slide& a, const Slide& b)
{
    return a.is_horizontal == b.is_horizontal
        && a.pic1 == b.pic1
        && (a.is_horizontal ? true : a.pic2 == b.pic2);
}

struct Solution2019 : SolutionBase
{
    std::vector<Slide> slides;
};

bool operator==(const Solution2019& a, const Solution2019& b)
{
    return a.score == b.score
       && a.slides == b.slides;
}

class Scorer2019 : public Scorer<Problem2019, Solution2019>
{
	using Score = size_t; // the scorer sets the nature of Score

	static const Score __ScoreType;

	public:

    static Score compute(const Problem2019& problem, const Solution2019& solution)
	{
		Score r = 0;
		tags_t tags1;
        if(solution.slides[0].is_horizontal)
		    tags1 = problem.pictures[solution.slides[0].pic1].tags;
		else
		{
		    const tags_t& tags_pic1 = problem.pictures[solution.slides[0].pic1].tags;
		    const tags_t& tags_pic2 = problem.pictures[solution.slides[0].pic2].tags;
		    set_union(tags_pic1, tags_pic2, tags1);
		}
		tags_t tags2;
		for(int i = 0; i < solution.slides.size() - 1; i++)
		{
		    if(solution.slides[i+1].is_horizontal)
		        tags2 = problem.pictures[solution.slides[i+1].pic1].tags;
		    else
		    {
		        const tags_t& tags_pic1 = problem.pictures[solution.slides[i+1].pic1].tags;
		        const tags_t& tags_pic2 = problem.pictures[solution.slides[i+1].pic2].tags;
		        tags2.reserve(tags_pic1.size() + tags_pic2.size());
		        set_union(tags_pic1, tags_pic2, tags2);
		    }

		    Score oneonly = 0, twoonly = 0, commons = 0;
		    set_difference_count(tags1, tags2, oneonly);
		    set_difference_count(tags2, tags1, twoonly);
		    set_intersection_count(tags1, tags2, commons);

		    r += min(oneonly, twoonly, commons);
		    tags1 = std::move(tags2);
		    tags2.clear();
		}
		return r;
	}
};

class GeneticAlgorithm : public GeneticAlgorithmBase<Problem2019, Solution2019>
{
public:
    using GeneticAlgorithmBase<Problem2019, Solution2019>::parse_problem;
    using GeneticAlgorithmBase<Problem2019, Solution2019>::dump_solution;
    using GeneticAlgorithmBase<Problem2019, Solution2019>::GeneticAlgorithmBase;
    using GeneticAlgorithmBase<Problem2019, Solution2019>::population;

    bool has_random() final { return true; }

    void random(Solution2019& to_randomize) final{

        std::vector<Slide>& slides = to_randomize.slides;
        slides.resize(problem.pictures.size() - problem.verticals.size()/2);

        static std::vector<pic_ind_t> verticals = problem.verticals;
        shuffle(verticals);

        pic_ind_t i;
        for(i = 0; i < verticals.size()/2; i++)
            slides[i] = Slide{false, verticals[2*i], verticals[2*i+1]};
        for(i = 0; i < problem.horizontals.size(); i++)
            slides[i + verticals.size()/2] = Slide{true, problem.horizontals[i], 0};

        shuffle(slides);

        to_randomize.score = Scorer2019::compute(problem, to_randomize);
    }

    bool has_crossover() final { return true; }

    void crossover(const Solution2019& parent1, const Solution2019& parent2, Solution2019& child1, Solution2019& child2) final
    {
        child1.slides.resize(parent1.slides.size());
        child2.slides.resize(parent2.slides.size());
        size_t j = 0;
        for(size_t i = 0; i < parent1.slides.size(); i++)
        {
            if(parent1.slides[i].is_horizontal)
            {
                child1.slides[i] = parent1.slides[i];
            }
            else
            {
                    while(parent2.slides[j].is_horizontal)
                    {
                        j++;
                    }
                    child1.slides[i] = parent2.slides[j++];
            }
        }
        j = 0;
        for(size_t i = 0; i < parent2.slides.size(); i++)
        {
            if(parent2.slides[i].is_horizontal)
            {
                child2.slides[i] = parent2.slides[i];
            }
            else
            {
                    while(parent1.slides[j].is_horizontal)
                    {
                        j++;
                    }
                    child2.slides[i] = parent1.slides[j++];
            }
        }
        child1.score = Scorer2019::compute(problem, child1);
        child2.score = Scorer2019::compute(problem, child2);
    }

    bool has_mutation() final { return true; }

    void mutation(const Solution2019& original, Solution2019& mutant) final {
        mutant = original;
        size_t first, second;
        first = Random::range(0ul, mutant.slides.size()-1);
        do {
            second = Random::range<size_t>(0, mutant.slides.size()-1);
        } while (first == second);

        std::swap(mutant.slides[first], mutant.slides[second]);
    }

    void dump_solution(std::ostream& out, const Solution2019& solution) const final
    {
        out << solution.slides.size();

        for(const Slide& s : solution.slides)
        {
            out << std::endl;
            out << s.pic1;
            if(!s.is_horizontal)
            {
                out << " " << s.pic2;
            }
        }
        out << std::endl;
    }

    bool has_parse_solution() final { return true; }
    void parse_solution(std::istream& in, Solution2019& solution) const final
    {
        std::string line;
        size_t size;
        in >> size;

        // To get rid of the rest of the size line
        std::getline(in, line);

        solution.slides.resize(size);
        size_t pic1, pic2;
        for(size_t i = 0; i < size; i++)
        {
            std::getline(in, line);

            std::stringstream ss(line);

            if(ss >> solution.slides[i].pic1)
            {
                if(ss >> solution.slides[i].pic2){ solution.slides[i].is_horizontal = false; }
                else { solution.slides[i].is_horizontal = true; }
            }
        }
        in >> solution.score;
    }

    void parse_problem(std::istream& in) final
    {
        std::set<std::string> viewed_tags;


        size_t n_pictures;
        in >> n_pictures;
        problem.pictures.reserve(n_pictures);

        for(size_t i = 0; i < n_pictures; i++)
        {
            std::string h, tag;
            size_t n_tags;
            size_t max_tag = 0;
            tags_t tags;

            in >> h >> n_tags;
            for(size_t j = 0; j < n_tags; j++)
            {
                in >> tag;
                if(viewed_tags.find(tag) == viewed_tags.end())
                {
                    viewed_tags.emplace(tag);
                    problem.tags_id[tag] = max_tag++;
                }

                tags.emplace_back(problem.tags_id[tag]);
            }

            sort(tags);

            bool is_h = h == "H";
            problem.pictures.emplace_back(Picture{is_h, tags});
            if(!is_h)
                problem.verticals.push_back(problem.pictures.size()-1);
            else
                problem.horizontals.push_back(problem.pictures.size()-1);
        }
    }
};

