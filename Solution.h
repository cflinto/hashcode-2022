#ifndef SOLUTION_H_INCLUDED
#define SOLUTION_H_INCLUDED


class SolutionBase
{
public:
	SolutionBase(){}
	SolutionBase(unsigned long score) : score(score) {}

    unsigned long score;
    inline bool operator<(const SolutionBase& rhs) { return score < rhs.score; }
};

#endif // SOLUTION_H_INCLUDED

