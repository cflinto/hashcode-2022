#ifndef EVOLUTION_H
#define EVOLUTION_H

#include <iostream>
#include <array>
#include <cassert>

#include "Solver.h"
#include "population.hpp"
#include "misc.h"

// *********
// * NOTES *
// *********
//
// - Mutation and Crossover are optional : this way, the algorithm can first run
// only on random/solver-based solutions and iteratively refine a first population
// while we try implementing those operations, hopefully offering us a headstart.
//
// - Populate and Step_Generation have default implementation, so we can use those
// and reimplement them differently if useful.

// Self-explanatory ?
using score_type      = uint;

// Abstract base class for genetic algorithm
// P : the type respresenting the Problem instance.
// S : the type representing Solutions.
template<class P, class S>
class GeneticAlgorithmBase : public Solver<P,S>
{
public:
    using Solver<P,S>::parse_problem;
    using Solver<P,S>::dump_solution;
    using Solver<P,S>::parameters;
    using Solver<P,S>::population;

    // Base constructor : record the problem instance and
    // parameters
    GeneticAlgorithmBase(const std::string& problem_path, const std::string& checkpoint_path, const GeneticParameters& parameters = GeneticParameters()) :
        Solver<P,S>(problem_path, checkpoint_path, parameters)
    {}

    // Step one generation forward
    void step_generation();

    void random_populate();

    // Breed a new generation
    // ie try to reach the target size by crossovers, mutations, and randoms
    void breed();

    void initialize() override { random_populate(); }
    void step() override { step_generation(); }

protected:
    void kill_all_clones() {
        population.kill(unique(population));
    }

    using Solver<P,S>::problem;

    // The selection step, optinally overridable.
    virtual void selection();

    // The roulette wheel selection step :
    // Keep individuals according to fitness proportionate probabilities
    population_iterator<S> roulette_wheel_selection(population_iterator<S> start, size_t lucky);

    // The Garip wheel selection step :
    // Keep individuals according to rank proportionate probabilities
    population_iterator<S> garip_wheel_selection(population_iterator<S> start, size_t lucky);

    virtual bool has_heuristic() { return false; }
    // The heuristic solution operation, optional
    virtual void heuristic(S& to_guess) { return; }

    virtual bool has_random() { return false; }
    // The random solution operation, optional
    virtual void random(S& to_randomize) { return; }

    // My black magic is weak in the C++14 land
    // Please give a better idea if you have one
    // The idea : allow for easy DCE in the algorithm
    virtual bool has_mutation() { return false; }
    // The mutation operation : optional
    // This implementation will never be used : dumb return to avoid warning.
    virtual void mutation(const S& original, S& mutant) { return; }

    // See has_mutation
    virtual bool has_crossover() { return false; }
    // The crossover operation : optional
    // This implementation will never be used : dumb return statement to avoid a warning.
    virtual void crossover(const S& parent1, const S& parent2, S& child1, S& child2) { return; }
};

template<class P, class S>
void GeneticAlgorithmBase<P,S>::random_populate()
{
    population.resize(parameters.target_size);
    size_t missing = parameters.target_size - population.alive();


    if(!(has_heuristic() || has_random()))
    {
        std::cerr << "WARNING: No heuristic nor random: can't populate." << std::endl;
        return;
    }

    size_t heuristics = parameters.heuristism * missing;
    if(!has_heuristic() && parameters.heuristism)
    {
        std::cerr << "WARNING: No heuristic implemented: heuristism is ignored." << std::endl;
        heuristics = 0;
    }

    if(!has_random() && parameters.heuristism)
    {
        std::cerr << "WARNING: No random implemented: random is ignored." << std::endl;
        heuristics = missing;
    }

    if(has_heuristic())
    {
        for(size_t i = 0; i < heuristics; i++)
        {
            // Create "population_size" solutions.
            // TODO : here handle the presence or not of a solver for better initialization ?
            heuristic(population.to_write());
        }
    }

    if(has_random())
    {
        size_t randoms = parameters.target_size - population.alive();
        for(size_t i = 0; i < randoms; i++)
        {
            random(population.to_write());
        }
    }

    population.sort();
}

template<class P, class S>
void GeneticAlgorithmBase<P,S>::selection()
{
    kill_all_clones();
    population.sort();

    // Apply parameters on current population
    size_t elite = parameters.elitism * population.alive();
    elite = max(elite, 1ul);
    size_t lucky = parameters.probabilism * population.alive();
    lucky = min(lucky, population.alive() - elite);

    // If we will end up selecting everyone:
    // Issue a warning and skip selection entirely
    if(elite + lucky >= population.alive())
    {
        std::cerr << "WARNING : elitism and probabilism are too high: no selection "
                     "is taking place." << std::endl;
        return;
    }

    // Elitist selection step : keep the best individuals
    auto it = population.begin() + elite;

    // Apply the selected probabilistic slection method next
    switch(parameters.selection)
    {
    case RouletteWheel:
        it = roulette_wheel_selection(it, lucky);
        break;
    case GaripWheel:
        it = garip_wheel_selection(it, lucky);
        break;
    default:
        break;

    }

    // Erase the unselected individuals :'(
    population.kill(it);
    population.sort();
}

template<class P, class S>
population_iterator<S> GeneticAlgorithmBase<P,S>::roulette_wheel_selection(population_iterator<S> start, size_t lucky)
{
    // Get the minimum score in the population
    score_type min_score = std::min(population.begin(), population.end())->score;
    // Compute an offset to apply to all other scores
    score_type offset = parameters.probabilist_param * min_score;
    // Try to avoid offsetting minimal scores to 0
    if(parameters.probabilist_param >= 0.0f && min_score > 0 && offset > 0)
        offset -= 1;
    auto add_scores = [](score_type a, const S& b){ return a + b.score; };
    for(;lucky > 0; lucky--)
    {
        // Compute the total score
        score_type total_score = std::accumulate(start, population.end(), 0, add_scores);
        // We want to remove offset from each score : let's remove it from the sum directly.
        total_score -= std::distance(start, population.end()) * offset;
        // Pick a point in this range
        score_type roulette = Random::range<score_type>(0, total_score);

        // Find where the ball ended..
        auto it = start;
        while(roulette > it->score - offset)
        {
            roulette -= it->score - offset;
            it++;
        }

        // Put the corresponding solution in the selected pool
        std::iter_swap(start, it);
        start++;

        // Resort the probabilistic pool by score for the next pick
        std::sort(population.rbegin(), std::make_reverse_iterator(start));
    }
    return start;
}

template<class P, class S>
population_iterator<S> GeneticAlgorithmBase<P,S>::garip_wheel_selection(population_iterator<S> start, size_t lucky)
{
    for(;lucky > 0; lucky--)
    {
        size_t pool_size = std::distance(start, population.end());
        // Compute the total score
        score_type rank_sum = pool_size*(pool_size+1)/2;
        // Pick a point in this range
        score_type roulette = Random::range<score_type>(0, rank_sum);

        // Find where the ball ended..
        auto it = start;
        while(roulette > pool_size)
        {
            roulette -= pool_size--;
            it++;
        }

        // Put the corresponding solution in the selected pool
        std::iter_swap(start, it);
        start++;

        // Resort the probabilistic pool by score for the next pick
        std::sort(population.rbegin(), std::make_reverse_iterator(start));
    }
    return start;
}

template<class P, class S>
void GeneticAlgorithmBase<P,S>::breed()
{
    // How many individuals do we need to breed ?
    size_t missing = parameters.target_size - population.alive();
    // How many were selected
    size_t breeders = population.alive();

    // How many by crossover ?
    size_t crossovers = parameters.crossoverism * missing;
    // Each crossover generate 2 individuals
    crossovers /= 2;
    // How many by mutations ?
    size_t mutations = parameters.mutationism * missing;

    // Warn if there is no crossover operation and crossoverism is used
    // Nested if for easier DCE (maybe ?)
    if(!has_crossover())
    {
        if(parameters.crossoverism >= 0.f)
        {
            std::cerr << "WARNING: no crossover operation: crossoverism ignored." << std::endl;
            crossovers = 0;
        }
    }

    if(!has_mutation())
    {
        if(parameters.mutationism >= 0.f)
        {
            std::cerr << "WARNING: no mutation operation: mutationism ignored." << std::endl;
            mutations = 0;
        }
    }

    // Check parameters validity
    if(2*crossovers + mutations >= missing)
    {
        std::cerr << "WARNING: crossoverism and mutationism too high: "
                     "They will be scaled, but this may prevent random "
                     "breeding." << std::endl;

        // Scale the parameters to avoid population overflow
        float scale = static_cast<float>(missing) /
                      static_cast<float>(2*crossovers + mutations);

        crossovers *= scale;
        mutations  *= scale;
    }

    // Do crossovers if implemented
    if(has_crossover())
    {
        std::vector<std::pair<size_t, size_t>> couples =
                Random::distinct_pairs<size_t>(crossovers, breeders-1);

        for(auto& couple : couples)
            crossover(population[couple.first], population[couple.second],
                      population.to_write(), population.to_write());
    }

    // Do mutations if implemented
    if(has_mutation())
    {
        std::vector<size_t> mutants =
                Random::distinct_integers<size_t>(mutations, 0, breeders-1);

        for(const size_t& mutant : mutants)
            mutation(population[mutant], population.to_write());
    }

    kill_all_clones();
    population.sort();

    random_populate();
}

template<class P, class S>
void GeneticAlgorithmBase<P,S>::step_generation()
{
    selection();

    breed();
}
#endif // EVOLUTION_H
