#include "competition_headers/2022practice.hpp"

int main(int argc, char** argv)
{
    if(argc != 2)
    {
        std::cerr << "Must have an argument (input file) !" << std::endl;
        exit(EXIT_FAILURE);
    }

    Random::Init();

    GeneticParameters parameters;

    parameters.target_size = 100;
    parameters.selection = RouletteWheel;

    parameters.elitism = 0.05f;
    parameters.probabilism = 0.25f;

    parameters.crossoverism = 0.48f;
    parameters.mutationism  = 0.48f;

    parameters.heuristism   = 0.25f;
    parameters.probabilist_param = 1.0f;


    const std::string& problem_filename = argv[1];
    const std::string& checkpoint_file = "check.point";
    GeneticAlgorithm gene_algo(problem_filename, checkpoint_file, parameters);

    Solver<Problem2022practice, Solution2022practice>* ptr = &gene_algo;

    ptr->parse_problem(problem_filename);

    ptr->recover(checkpoint_file);

    std::cout << "Populating... ";

    ptr->initialize();

    std::cout << "Done !" << std::endl;

    ptr->process();

    for(size_t i = 0; ; i++)
    {
        ptr->step();

        ptr->process();

        ptr->dump_best(problem_filename + ".out.txt");
    }
}
