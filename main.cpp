#include "Solution.h"
#include "Problem.h"
#include "Solver.h"
#include "Scorer.h"
#include "Strategy.h"
#include "helpers/random.h"

#include "competition_headers/2019.hpp"

#include "genetic/genetic_algorithm.hpp"

#include <algorithm>
#include <numeric>

#include <starpu.h>
#include <starpu_mpi.h>
#include <mpi.h>

void migrationShuffleFunction(void** buffers, void* arg)
{
    std::pair<int,int>* pairs = (std::pair<int,int>*)STARPU_VECTOR_GET_PTR(buffers[0]);
    int size;
    starpu_codelet_unpack_args(arg, &size);
    std::shuffle(pairs, pairs + size, Random::generator());
}

int main(int argc, char** argv)
{
    // Check argument presence
    int world_rank, world_size;
    if(argc != 2)
    {
        std::cerr << "Must have an argument (input file) !" << std::endl;
        exit(EXIT_FAILURE);
    }
    // Initialize RNG
    Random::Init();

    int provided;
    // Initialize MPI
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Initialize StarPU-MPI
    int ret;
    struct starpu_conf conf;
    starpu_conf_init(&conf);

    // Use this to reserve different amount of CPUs per node
//    conf.reserve_ncpus = world_rank%3;
    if (ret == -ENODEV)
        return 77;

    ret = starpu_mpi_init_conf(&argc, &argv, 0, MPI_COMM_WORLD, &conf);
    STARPU_CHECK_RETURN_VALUE(ret, "starpu_mpi_init_conf");
    assert(ret == 0);

    // Now all nodes need to agree on every node's number of workers
    // and ids

    //First, gather numbers of workers of every nodes
    std::vector<int> workers(world_size);
    workers[world_rank] = starpu_worker_get_count_by_type(STARPU_ANY_WORKER);
    MPI_Allgather(workers.data()+world_rank, 1, MPI_INT,
                  workers.data(), 1, MPI_INT,
                  MPI_COMM_WORLD);

    // Then get local workers ids
    std::vector<int> local_ids(workers[world_rank]);
    starpu_worker_get_ids_by_type(STARPU_ANY_WORKER,
                                  local_ids.data(),
                                  local_ids.size());


    //Then, gather all their ids
    std::vector<int> lower_workers(world_size+1);
    lower_workers[0] = 0;
    std::partial_sum(std::begin(workers), std::end(workers), std::begin(lower_workers)+1);
    int total_workers = std::accumulate(std::begin(workers), std::end(workers), 0);
    std::vector<std::pair<int,int>> workers_id(total_workers);
    for(int i = 0; i < workers[world_rank]; i++)
        workers_id[lower_workers[world_rank]+i] = {world_rank, local_ids[i]};

    for(auto& worker : workers)
        worker *= 2;
    for(auto& lworker : lower_workers)
        lworker *= 2;
    MPI_Allgatherv(MPI_IN_PLACE, 0, MPI_DATATYPE_NULL,
                   workers_id.data(), workers.data(), lower_workers.data(), MPI_INT,
                   MPI_COMM_WORLD);
    for(auto& worker : workers)
        worker /= 2;
    for(auto& lworker : lower_workers)
        lworker /= 2;
    std::cout << "Node " << world_rank << " has list workers : {";
    for(auto& id : workers_id)
        std::cout << id.first << ':' << id.second << ' ';
    std::cout << '}' << std::endl;



	using MyProblemClass = Problem2019;
    using MyScorerClass = Scorer2019;
    using MySolutionClass = Solution2019;
	//using MySolverClass = GeneticAlgorithmBase<MyProblemClass, MySolutionClass>;
    using MySolverClass = GeneticAlgorithm;

	using __Solution = MySolutionClass;
	using __Problem = MyProblemClass;
    using __Solver = MySolverClass;
    using __Population = Population<__Solution>;

    using __Strategy = Strategy<__Solution, __Problem, __Solver>;

    const std::string problem_path(argv[1]);

    // Initialize a strategy for every worker
    std::vector<std::vector<__Strategy>> strategies(world_size);
    for(auto& id : workers_id)
        strategies[id.first].emplace_back(id.first, id.second, total_workers);

    // Initialize parameters for every worker
    for(auto& node_strategies : strategies)
        for(auto& strategy : node_strategies)
        {
            starpu_data_acquire(strategy.handleParams, STARPU_W);

            GeneticParameters& params = *(GeneticParameters*)starpu_variable_get_local_ptr(strategy.handleParams);

            params = GeneticParameters();
            params.heuristism = 0.f;
            starpu_data_release(strategy.handleParams);
        }

    // Actually initialize all Solvers, parallely
    for(auto& node_strategies : strategies)
        for(auto& strategy : node_strategies)
            strategy.initialize(problem_path);

    // Prepare data handle for the reduction-selected
    // champion buffer
    starpu_data_handle_t handleChampion;
    starpu_variable_data_register(&handleChampion, -1, (uintptr_t)0, sizeof(Champion_ID));

    // Now prepare its reduction methods.
    starpu_codelet codeletChampionIDNeutral = make_CPU_codelet("ChampionIDNeutral",
                                                               champion_id_neutral,
                                                               STARPU_W);
    starpu_codelet codeletChampionIDRedux   = make_CPU_codelet("ChampionIDRedux",
                                                               champion_id_redux,
                                                               STARPU_RW,
                                                               STARPU_R);
    starpu_data_set_reduction_methods(handleChampion,
                                      &codeletChampionIDRedux,
                                      &codeletChampionIDNeutral);
    // Register the champion handle on the last node
    int tag_champion = 8 * world_size * total_workers;
    starpu_mpi_data_register(handleChampion, tag_champion, world_size-1);

    // Register a data_handle_t for migration shuffling
    starpu_data_handle_t handleMigration;
    starpu_vector_data_register(&handleMigration, -1, (uintptr_t)0, total_workers, sizeof(std::pair<int,int>));
    starpu_mpi_data_register(handleMigration, tag_champion+1, world_size-1);

    starpu_codelet codeletMigrationShuffle = make_CPU_codelet("migrationShuffle",
                                                              migrationShuffleFunction,
                                                              STARPU_RW);

    if(world_rank == world_size-1)
    {
    starpu_data_acquire(handleMigration, STARPU_W);

        std::pair<int, int>* pairs = (std::pair<int,int>*)starpu_vector_get_local_ptr(handleMigration);
        for(size_t i = 0; i < total_workers; i++)
            pairs[i] = workers_id[i];

    starpu_data_release(handleMigration);
    }

    // TODO : change to clean score type
    size_t last_best = 0;

    // Yup...
    for(size_t iteration = 1; ;iteration++)
    {

        // Do one step on each strategy
        for(auto& node_strategies : strategies)
            for(auto& strategy : node_strategies)
                strategy.step();

        // Do one process on each strategy
        // This is where they write to their migration buffer
        for(auto& node_strategies : strategies)
            for(auto& strategy : node_strategies)
                strategy.process();

        // Shuffle the migration !
        starpu_mpi_task_insert(MPI_COMM_WORLD,
                               &codeletMigrationShuffle,
                               STARPU_RW, handleMigration,
                               STARPU_VALUE, &total_workers, sizeof(total_workers),
                               0);


        for(int i = 0; i < world_size; i++)
            starpu_mpi_get_data_on_node(MPI_COMM_WORLD, handleMigration, i);

        // Do one intake per Strategy
        // This is where they read from one another's
        // migration buffer

        starpu_data_acquire(handleMigration, STARPU_R);
        std::pair<int, int>* pairs = (std::pair<int, int>*) starpu_vector_get_local_ptr(handleMigration);

        size_t k = 0;
        for(size_t i = 0; i < world_size; i++)
            for(size_t j = 0; j < workers[i]; j++)
            {
                strategies[i][j].intake(strategies[pairs[k].first][pairs[k].second].handleBuffer);
                k++;
            }
        starpu_data_release(handleMigration);

        // All Strategies should now prepare their own Champion's ID
        // For reduction
        for(auto& node_strategies : strategies)
            for(auto& strategy : node_strategies)
                strategy.prepareChampionID(handleChampion);
        starpu_mpi_redux_data(MPI_COMM_WORLD, handleChampion);

        for(int i = 0; i < world_size; i++)
            starpu_mpi_get_data_on_node(MPI_COMM_WORLD, handleChampion, i);

        starpu_data_acquire(handleChampion, STARPU_R);
        // Now we have the global champion's location
        Champion_ID& champion_id = *(Champion_ID*)starpu_vector_get_local_ptr(handleChampion);
        // Check if it's better and record it
        if(champion_id.score > last_best)
        {
            last_best = champion_id.score;
            if(world_rank == 0)
            {
                std::cout << "New best (" << last_best << ") found by worker "
                          << champion_id.world_rank << ':' << champion_id.worker_id
                          << std::endl;
            }

            //Weird but meh, working!
            __Strategy& strat = strategies[champion_id.world_rank]
                                          [champion_id.worker_id];
            strat.dumpChampion();

        }


        starpu_data_release(handleChampion);

        // Wait before going for another loop to not
        // overweight on the scheduler (is he even working actually ?)
        // We are giving explicit workers to each task.
        // Doc says this "bypass" the scheduler...

        starpu_task_wait_for_n_submitted(10 * total_workers);
    }

	fprintf(stderr, "Wait task\n");
	starpu_task_wait_for_all();

	fprintf(stderr, "Release data\n");
	//for(int idxHandle = 0 ; idxHandle < nbHandles ; ++idxHandle)
	{
		//starpu_data_unregister(handles[idxHandle]);
	}

	fprintf(stderr, "Shutdown\n");

	starpu_shutdown();
}

